[002169E0]79E0 [20 AA DA]: JSR $DAAA @ 转子$00E88AAA
[002169E3]79E3 [08 78 38]: PHP @ 处理器状态压入堆栈
[002169E4]79E4 [78 38 A5]: SEI @ 置中断禁止位
[002169E5]79E5 [38 A5 28]: SEC @ 置进位标志
[002169E6]79E6 [A5 28 E9]: LDA $28 = #$CF @ 送累加器
[002169E8]79E8 [E9 0F 85]: SBC #$0F @ 带借位的减法
[002169EA]79EA [85 28 A5]: STA $28 = #$C0 @ 存累加器
[002169EC]79EC [A5 29 E9]: LDA $29 = #$17 @ 送累加器
[002169EE]79EE [E9 00 85]: SBC #$00 @ 带借位的减法
[002169F0]79F0 [85 29 28]: STA $29 = #$17 @ 存累加器
[002169F2]79F2 [28 AD 83]: PLP @ 堆栈弹回处理器状态
[002169F3]79F3 [AD 83 19]: LDA $1983 = #$00 @ 送累加器
[002169F6]79F6 [48 A9 03]: PHA @ 累加器压入堆栈
[002169F7]79F7 [A9 03 85]: LDA #$03 @ 送累加器
[002169F9]79F9 [85 23 68]: STA $23 = #$03 @ 存累加器
[002169FB]79FB [68 20 E1]: PLA @ 堆栈弹回累加器
[002169FC]79FC [20 E1 DB]: JSR $DBE1 @ 转子$00E88BE1
[002169FF]79FF [A0 06 91]: LDY #$06 @ 送变址寄存器Y
[00216A01]7A01 [91 28 AD]: STA ($28),Y @ $17C6 = #$00 @ 存累加器
[00216A03]7A03 [AD 84 19]: LDA $1984 = #$00 @ 送累加器
[00216A06]7A06 [85 23 A0]: STA $23 = #$00 @ 存累加器
[00216A08]7A08 [A0 06 B1]: LDY #$06 @ 送变址寄存器Y
[00216A0A]7A0A [B1 28 20]: LDA ($28),Y @ $17C6 = #$00 @ 送累加器
[00216A0C]7A0C [20 EF DC]: JSR $DCEF @ 转子$00E88CEF
[00216A0F]7A0F [A0 06 91]: LDY #$06 @ 送变址寄存器Y
[00216A11]7A11 [91 28 AD]: STA ($28),Y @ $17C6 = #$00 @ 存累加器
[00216A13]7A13 [AD 83 19]: LDA $1983 = #$00 @ 送累加器
[00216A16]7A16 [38 E9 01]: SEC @ 置进位标志
[00216A17]7A17 [E9 01 A0]: SBC #$01 @ 带借位的减法
[00216A19]7A19 [A0 03 91]: LDY #$03 @ 送变址寄存器Y
[00216A1B]7A1B [91 28 AD]: STA ($28),Y @ $17C3 = #$FF @ 存累加器
[00216A1D]7A1D [AD 84 19]: LDA $1984 = #$00 @ 送累加器
[00216A20]7A20 [38 E9 01]: SEC @ 置进位标志
[00216A21]7A21 [E9 01 A0]: SBC #$01 @ 带借位的减法
[00216A23]7A23 [A0 02 91]: LDY #$02 @ 送变址寄存器Y
[00216A25]7A25 [91 28 A0]: STA ($28),Y @ $17C2 = #$FF @ 存累加器
[00216A27]7A27 [A0 09 A9]: LDY #$09 @ 送变址寄存器Y
[00216A29]7A29 [A9 00 91]: LDA #$00 @ 送累加器
[00216A2B]7A2B [91 28 A9]: STA ($28),Y @ $17C9 = #$00 @ 存累加器
[00216A2D]7A2D [A9 90 C8]: LDA #$90 @ 送累加器
[00216A2F]7A2F [C8 91 28]: INY @ 变址寄存器Y加1
[00216A30]7A30 [91 28 AD]: STA ($28),Y @ $17CA = #$90 @ 存累加器
[00216A32]7A32 [AD 86 19]: LDA $1986 = #$00 @ 送累加器
[00216A35]7A35 [85 20 AD]: STA $20 = #$00 @ 存累加器
[00216A37]7A37 [AD 87 19]: LDA $1987 = #$00 @ 送累加器
[00216A3A]7A3A [85 21 A5]: STA $21 = #$00 @ 存累加器
[00216A3C]7A3C [A5 20 85]: LDA $20 = #$00 @ 送累加器
[00216A3E]7A3E [85 23 A5]: STA $23 = #$00 @ 存累加器
[00216A40]7A40 [A5 21 85]: LDA $21 = #$00 @ 送累加器
[00216A42]7A42 [85 24 A0]: STA $24 = #$00 @ 存累加器
[00216A44]7A44 [A0 09 B1]: LDY #$09 @ 送变址寄存器Y
[00216A46]7A46 [B1 28 85]: LDA ($28),Y @ $17C9 = #$00 @ 送累加器
[00216A48]7A48 [85 20 C8]: STA $20 = #$00 @ 存累加器
[00216A4A]7A4A [C8 B1 28]: INY @ 变址寄存器Y加1
[00216A4B]7A4B [B1 28 85]: LDA ($28),Y @ $17CA = #$90 @ 送累加器
[00216A4D]7A4D [85 21 18]: STA $21 = #$90 @ 存累加器
[00216A4F]7A4F [18 A5 20]: CLC @ 清进位标志
[00216A50]7A50 [A5 20 65]: LDA $20 = #$00 @ 送累加器
[00216A52]7A52 [65 23 85]: ADC $23 = #$00 @ 带进位加
[00216A54]7A54 [85 20 A5]: STA $20 = #$00 @ 存累加器
[00216A56]7A56 [A5 21 65]: LDA $21 = #$90 @ 送累加器
[00216A58]7A58 [65 24 85]: ADC $24 = #$00 @ 带进位加
[00216A5A]7A5A [85 21 A0]: STA $21 = #$90 @ 存累加器
[00216A5C]7A5C [A0 09 A5]: LDY #$09 @ 送变址寄存器Y
[00216A5E]7A5E [A5 20 91]: LDA $20 = #$00 @ 送累加器
[00216A60]7A60 [91 28 C8]: STA ($28),Y @ $17C9 = #$00 @ 存累加器
[00216A62]7A62 [C8 A5 21]: INY @ 变址寄存器Y加1
[00216A63]7A63 [A5 21 91]: LDA $21 = #$90 @ 送累加器
[00216A65]7A65 [91 28 A9]: STA ($28),Y @ $17CA = #$90 @ 存累加器
[00216A67]7A67 [A9 04 85]: LDA #$04 @ 送累加器
[00216A69]7A69 [85 23 A9]: STA $23 = #$04 @ 存累加器
[00216A6B]7A6B [A9 00 85]: LDA #$00 @ 送累加器
[00216A6D]7A6D [85 24 A0]: STA $24 = #$00 @ 存累加器
[00216A6F]7A6F [A0 09 B1]: LDY #$09 @ 送变址寄存器Y
[00216A71]7A71 [B1 28 85]: LDA ($28),Y @ $17C9 = #$00 @ 送累加器
[00216A73]7A73 [85 20 C8]: STA $20 = #$00 @ 存累加器
[00216A75]7A75 [C8 B1 28]: INY @ 变址寄存器Y加1
[00216A76]7A76 [B1 28 85]: LDA ($28),Y @ $17CA = #$90 @ 送累加器
[00216A78]7A78 [85 21 18]: STA $21 = #$90 @ 存累加器
[00216A7A]7A7A [18 A5 20]: CLC @ 清进位标志
[00216A7B]7A7B [A5 20 65]: LDA $20 = #$00 @ 送累加器
[00216A7D]7A7D [65 23 85]: ADC $23 = #$04 @ 带进位加
[00216A7F]7A7F [85 20 A5]: STA $20 = #$04 @ 存累加器
[00216A81]7A81 [A5 21 65]: LDA $21 = #$90 @ 送累加器
[00216A83]7A83 [65 24 85]: ADC $24 = #$00 @ 带进位加
[00216A85]7A85 [85 21 A0]: STA $21 = #$90 @ 存累加器
[00216A87]7A87 [A0 09 A5]: LDY #$09 @ 送变址寄存器Y
[00216A89]7A89 [A5 20 91]: LDA $20 = #$04 @ 送累加器
[00216A8B]7A8B [91 28 C8]: STA ($28),Y @ $17C9 = #$04 @ 存累加器
[00216A8D]7A8D [C8 A5 21]: INY @ 变址寄存器Y加1
[00216A8E]7A8E [A5 21 91]: LDA $21 = #$90 @ 送累加器
[00216A90]7A90 [91 28 A0]: STA ($28),Y @ $17CA = #$90 @ 存累加器
[00216A92]7A92 [A0 09 B1]: LDY #$09 @ 送变址寄存器Y
[00216A94]7A94 [B1 28 AA]: LDA ($28),Y @ $17C9 = #$04 @ 送累加器
[00216A96]7A96 [AA C8 B1]: TAX @ 累加器送变址寄存器X
[00216A97]7A97 [C8 B1 28]: INY @ 变址寄存器Y加1
[00216A98]7A98 [B1 28 85]: LDA ($28),Y @ $17CA = #$90 @ 送累加器
[00216A9A]7A9A [85 27 86]: STA $27 = #$90 @ 存累加器
[00216A9C]7A9C [86 26 A0]: STX $26 = #$04 @ 存变址寄存器X
[00216A9E]7A9E [A0 00 B1]: LDY #$00 @ 送变址寄存器Y
[00216AA0]7AA0 [B1 26 A0]: LDA ($26),Y @ $9004 = #$00 @ 送累加器
[00216AA2]7AA2 [A0 07 91]: LDY #$07 @ 送变址寄存器Y
[00216AA4]7AA4 [91 28 A0]: STA ($28),Y @ $17C7 = #$00 @ 存累加器
[00216AA6]7AA6 [A0 09 18]: LDY #$09 @ 送变址寄存器Y
[00216AA8]7AA8 [18 B1 28]: CLC @ 清进位标志
[00216AA9]7AA9 [B1 28 69]: LDA ($28),Y @ $17C9 = #$04 @ 送累加器
[00216AAB]7AAB [69 01 91]: ADC #$01 @ 带进位加
[00216AAD]7AAD [91 28 91]: STA ($28),Y @ $17C9 = #$05 @ 存累加器
[00216AAF]7AAF [91 28 C8]: STA ($28),Y @ $17C9 = #$05 @ 存累加器
[00216AB1]7AB1 [C8 B1 28]: INY @ 变址寄存器Y加1
[00216AB2]7AB2 [B1 28 69]: LDA ($28),Y @ $17CA = #$90 @ 送累加器
[00216AB4]7AB4 [69 00 91]: ADC #$00 @ 带进位加
[00216AB6]7AB6 [91 28 A0]: STA ($28),Y @ $17CA = #$90 @ 存累加器
[00216AB8]7AB8 [A0 09 B1]: LDY #$09 @ 送变址寄存器Y
[00216ABA]7ABA [B1 28 AA]: LDA ($28),Y @ $17C9 = #$05 @ 送累加器
[00216ABC]7ABC [AA C8 B1]: TAX @ 累加器送变址寄存器X
[00216ABD]7ABD [C8 B1 28]: INY @ 变址寄存器Y加1
[00216ABE]7ABE [B1 28 85]: LDA ($28),Y @ $17CA = #$90 @ 送累加器
[00216AC0]7AC0 [85 27 86]: STA $27 = #$90 @ 存累加器
[00216AC2]7AC2 [86 26 A0]: STX $26 = #$05 @ 存变址寄存器X
[00216AC4]7AC4 [A0 00 B1]: LDY #$00 @ 送变址寄存器Y
[00216AC6]7AC6 [B1 26 A0]: LDA ($26),Y @ $9005 = #$D2 @ 送累加器
[00216AC8]7AC8 [A0 08 91]: LDY #$08 @ 送变址寄存器Y
[00216ACA]7ACA [91 28 A0]: STA ($28),Y @ $17C8 = #$D2 @ 存累加器
[00216ACC]7ACC [A0 09 18]: LDY #$09 @ 送变址寄存器Y
[00216ACE]7ACE [18 B1 28]: CLC @ 清进位标志
[00216ACF]7ACF [B1 28 69]: LDA ($28),Y @ $17C9 = #$05 @ 送累加器
[00216AD1]7AD1 [69 01 91]: ADC #$01 @ 带进位加
[00216AD3]7AD3 [91 28 91]: STA ($28),Y @ $17C9 = #$06 @ 存累加器
[00216AD5]7AD5 [91 28 C8]: STA ($28),Y @ $17C9 = #$06 @ 存累加器
[00216AD7]7AD7 [C8 B1 28]: INY @ 变址寄存器Y加1
[00216AD8]7AD8 [B1 28 69]: LDA ($28),Y @ $17CA = #$90 @ 送累加器
[00216ADA]7ADA [69 00 91]: ADC #$00 @ 带进位加
[00216ADC]7ADC [91 28 A0]: STA ($28),Y @ $17CA = #$90 @ 存累加器
[00216ADE]7ADE [A0 03 B1]: LDY #$03 @ 送变址寄存器Y
[00216AE0]7AE0 [B1 28 18]: LDA ($28),Y @ $17C3 = #$FF @ 送累加器
[00216AE2]7AE2 [18 69 01]: CLC @ 清进位标志
[00216AE3]7AE3 [69 01 48]: ADC #$01 @ 带进位加
[00216AE5]7AE5 [48 A0 0F]: PHA @ 累加器压入堆栈
[00216AE6]7AE6 [A0 0F B1]: LDY #$0F @ 送变址寄存器Y
[00216AE8]7AE8 [B1 28 85]: LDA ($28),Y @ $17CF = #$00 @ 送累加器
[00216AEA]7AEA [85 23 68]: STA $23 = #$00 @ 存累加器
[00216AEC]7AEC [68 20 EF]: PLA @ 堆栈弹回累加器
[00216AED]7AED [20 EF DC]: JSR $DCEF @ 转子$00E88CEF
[00216AF0]7AF0 [18 69 08]: CLC @ 清进位标志
[00216AF1]7AF1 [69 08 A0]: ADC #$08 @ 带进位加
[00216AF3]7AF3 [A0 05 91]: LDY #$05 @ 送变址寄存器Y
[00216AF5]7AF5 [91 28 AD]: STA ($28),Y @ $17C5 = #$08 @ 存累加器
[00216AF7]7AF7 [AD 84 19]: LDA $1984 = #$00 @ 送累加器
[00216AFA]7AFA [85 23 A0]: STA $23 = #$00 @ 存累加器
[00216AFC]7AFC [A0 10 B1]: LDY #$10 @ 送变址寄存器Y
[00216AFE]7AFE [B1 28 20]: LDA ($28),Y @ $17D0 = #$00 @ 送累加器
[00216B00]7B00 [20 EF DC]: JSR $DCEF @ 转子$00E88CEF
[00216B03]7B03 [18 69 00]: CLC @ 清进位标志
[00216B04]7B04 [69 00 A0]: ADC #$00 @ 带进位加
[00216B06]7B06 [A0 04 91]: LDY #$04 @ 送变址寄存器Y
[00216B08]7B08 [91 28 A9]: STA ($28),Y @ $17C4 = #$00 @ 存累加器
[00216B0A]7B0A [A9 09 85]: LDA #$09 @ 送累加器
[00216B0C]7B0C [85 23 A9]: STA $23 = #$09 @ 存累加器
[00216B0E]7B0E [A9 00 85]: LDA #$00 @ 送累加器
[00216B10]7B10 [85 24 A0]: STA $24 = #$00 @ 存累加器
[00216B12]7B12 [A0 10 B1]: LDY #$10 @ 送变址寄存器Y
[00216B14]7B14 [B1 28 85]: LDA ($28),Y @ $17D0 = #$00 @ 送累加器
[00216B16]7B16 [85 20 A9]: STA $20 = #$00 @ 存累加器
[00216B18]7B18 [A9 00 85]: LDA #$00 @ 送累加器
[00216B1A]7B1A [85 21 20]: STA $21 = #$00 @ 存累加器
[00216B1C]7B1C [20 AF D6]: JSR $D6AF @ 转子$00E886AF
[00216B1F]7B1F [A5 24 48]: LDA $24 = #$00 @ 送累加器
[00216B21]7B21 [48 A5 23]: PHA @ 累加器压入堆栈
[00216B22]7B22 [A5 23 48]: LDA $23 = #$09 @ 送累加器
[00216B24]7B24 [48 A9 02]: PHA @ 累加器压入堆栈
[00216B25]7B25 [A9 02 85]: LDA #$02 @ 送累加器
[00216B27]7B27 [85 23 A9]: STA $23 = #$02 @ 存累加器
[00216B29]7B29 [A9 00 85]: LDA #$00 @ 送累加器
[00216B2B]7B2B [85 24 20]: STA $24 = #$00 @ 存累加器
[00216B2D]7B2D [20 AF D6]: JSR $D6AF @ 转子$00E886AF
[00216B30]7B30 [68 85 23]: PLA @ 堆栈弹回累加器
[00216B31]7B31 [85 23 68]: STA $23 = #$09 @ 存累加器
[00216B33]7B33 [68 85 24]: PLA @ 堆栈弹回累加器
[00216B34]7B34 [85 24 18]: STA $24 = #$00 @ 存累加器
[00216B36]7B36 [18 A5 20]: CLC @ 清进位标志
[00216B37]7B37 [A5 20 69]: LDA $20 = #$00 @ 送累加器
[00216B39]7B39 [69 23 85]: ADC #$23 @ 带进位加
[00216B3B]7B3B [85 20 A5]: STA $20 = #$23 @ 存累加器
[00216B3D]7B3D [A5 21 69]: LDA $21 = #$00 @ 送累加器
[00216B3F]7B3F [69 1A 85]: ADC #$1A @ 带进位加
[00216B41]7B41 [85 21 A0]: STA $21 = #$1A @ 存累加器
[00216B43]7B43 [A0 0B A5]: LDY #$0B @ 送变址寄存器Y
[00216B45]7B45 [A5 20 91]: LDA $20 = #$23 @ 送累加器
[00216B47]7B47 [91 28 C8]: STA ($28),Y @ $17CB = #$23 @ 存累加器
[00216B49]7B49 [C8 A5 21]: INY @ 变址寄存器Y加1
[00216B4A]7B4A [A5 21 91]: LDA $21 = #$1A @ 送累加器
[00216B4C]7B4C [91 28 A0]: STA ($28),Y @ $17CC = #$1A @ 存累加器
[00216B4E]7B4E [A0 0F B1]: LDY #$0F @ 送变址寄存器Y
[00216B50]7B50 [B1 28 85]: LDA ($28),Y @ $17CF = #$00 @ 送累加器
[00216B52]7B52 [85 20 A9]: STA $20 = #$00 @ 存累加器
[00216B54]7B54 [A9 00 85]: LDA #$00 @ 送累加器
[00216B56]7B56 [85 21 A5]: STA $21 = #$00 @ 存累加器
[00216B58]7B58 [A5 24 48]: LDA $24 = #$00 @ 送累加器
[00216B5A]7B5A [48 A5 23]: PHA @ 累加器压入堆栈
[00216B5B]7B5B [A5 23 48]: LDA $23 = #$09 @ 送累加器
[00216B5D]7B5D [48 A9 02]: PHA @ 累加器压入堆栈
[00216B5E]7B5E [A9 02 85]: LDA #$02 @ 送累加器
[00216B60]7B60 [85 23 A9]: STA $23 = #$02 @ 存累加器
[00216B62]7B62 [A9 00 85]: LDA #$00 @ 送累加器
[00216B64]7B64 [85 24 20]: STA $24 = #$00 @ 存累加器
[00216B66]7B66 [20 AF D6]: JSR $D6AF @ 转子$00E886AF
[00216B69]7B69 [68 85 23]: PLA @ 堆栈弹回累加器
[00216B6A]7B6A [85 23 68]: STA $23 = #$09 @ 存累加器
[00216B6C]7B6C [68 85 24]: PLA @ 堆栈弹回累加器
[00216B6D]7B6D [85 24 A0]: STA $24 = #$00 @ 存累加器
[00216B6F]7B6F [A0 0B 18]: LDY #$0B @ 送变址寄存器Y
[00216B71]7B71 [18 B1 28]: CLC @ 清进位标志
[00216B72]7B72 [B1 28 65]: LDA ($28),Y @ $17CB = #$23 @ 送累加器
[00216B74]7B74 [65 20 85]: ADC $20 = #$00 @ 带进位加
[00216B76]7B76 [85 20 C8]: STA $20 = #$23 @ 存累加器
[00216B78]7B78 [C8 B1 28]: INY @ 变址寄存器Y加1
[00216B79]7B79 [B1 28 65]: LDA ($28),Y @ $17CC = #$1A @ 送累加器
[00216B7B]7B7B [65 21 85]: ADC $21 = #$00 @ 带进位加
[00216B7D]7B7D [85 21 A0]: STA $21 = #$1A @ 存累加器
[00216B7F]7B7F [A0 00 B1]: LDY #$00 @ 送变址寄存器Y
[00216B81]7B81 [B1 20 85]: LDA ($20),Y @ $1A23 = #$00 @ 送累加器
[00216B83]7B83 [85 20 A9]: STA $20 = #$00 @ 存累加器
[00216B85]7B85 [A9 00 85]: LDA #$00 @ 送累加器
[00216B87]7B87 [85 21 A0]: STA $21 = #$00 @ 存累加器
[00216B89]7B89 [A0 00 A5]: LDY #$00 @ 送变址寄存器Y
[00216B8B]7B8B [A5 20 91]: LDA $20 = #$00 @ 送累加器
[00216B8D]7B8D [91 28 C8]: STA ($28),Y @ $17C0 = #$00 @ 存累加器
[00216B8F]7B8F [C8 A5 21]: INY @ 变址寄存器Y加1
[00216B90]7B90 [A5 21 91]: LDA $21 = #$00 @ 送累加器
[00216B92]7B92 [91 28 A9]: STA ($28),Y @ $17C1 = #$00 @ 存累加器
[00216B94]7B94 [A9 80 85]: LDA #$80 @ 送累加器
[00216B96]7B96 [85 23 A9]: STA $23 = #$80 @ 存累加器
[00216B98]7B98 [A9 00 85]: LDA #$00 @ 送累加器
[00216B9A]7B9A [85 24 A0]: STA $24 = #$00 @ 存累加器
[00216B9C]7B9C [A0 00 B1]: LDY #$00 @ 送变址寄存器Y
[00216B9E]7B9E [B1 28 85]: LDA ($28),Y @ $17C0 = #$00 @ 送累加器
[00216BA0]7BA0 [85 20 C8]: STA $20 = #$00 @ 存累加器
[00216BA2]7BA2 [C8 B1 28]: INY @ 变址寄存器Y加1
[00216BA3]7BA3 [B1 28 85]: LDA ($28),Y @ $17C1 = #$00 @ 送累加器
[00216BA5]7BA5 [85 21 20]: STA $21 = #$00 @ 存累加器
[00216BA7]7BA7 [20 40 D3]: JSR $D340 @ 转子$00E88340
[00216BAA]7BAA [B0 03 4C]: BCS $7BAF @ 进位为“1”分支
[00216BAC]7BAC [4C D7 7B]: JMP $7BD7 @ 转移$00216BD7
[00216BAF]7BAF [D0 03 4C]: BNE $7BB4 @ 结果不为0分支
[00216BB1]7BB1 [4C D7 7B]: JMP $7BD7 @ 转移$00216BD7
[00216BB4]7BB4 [A0 00 B1]: LDY #$00 @ 送变址寄存器Y
[00216BB6]7BB6 [B1 28 85]: LDA ($28),Y @ $17C0 = #$00 @ 送累加器
[00216BB8]7BB8 [85 20 C8]: STA $20 = #$00 @ 存累加器
[00216BBA]7BBA [C8 B1 28]: INY @ 变址寄存器Y加1
[00216BBB]7BBB [B1 28 85]: LDA ($28),Y @ $17C1 = #$00 @ 送累加器
[00216BBD]7BBD [85 21 A5]: STA $21 = #$00 @ 存累加器
[00216BBF]7BBF [A5 20 38]: LDA $20 = #$00 @ 送累加器
[00216BC1]7BC1 [38 E9 80]: SEC @ 置进位标志
[00216BC2]7BC2 [E9 80 85]: SBC #$80 @ 带借位的减法
[00216BC4]7BC4 [85 20 A5]: STA $20 = #$80 @ 存累加器
[00216BC6]7BC6 [A5 21 E9]: LDA $21 = #$00 @ 送累加器
[00216BC8]7BC8 [E9 00 85]: SBC #$00 @ 带借位的减法
[00216BCA]7BCA [85 21 A0]: STA $21 = #$FF @ 存累加器
[00216BCC]7BCC [A0 00 A5]: LDY #$00 @ 送变址寄存器Y
[00216BCE]7BCE [A5 20 91]: LDA $20 = #$80 @ 送累加器
[00216BD0]7BD0 [91 28 C8]: STA ($28),Y @ $17C0 = #$80 @ 存累加器
[00216BD2]7BD2 [C8 A5 21]: INY @ 变址寄存器Y加1
[00216BD3]7BD3 [A5 21 91]: LDA $21 = #$FF @ 送累加器
[00216BD5]7BD5 [91 28 A0]: STA ($28),Y @ $17C1 = #$FF @ 存累加器
[00216BD7]7BD7 [A0 06 B1]: LDY #$06 @ 送变址寄存器Y
[00216BD9]7BD9 [B1 28 85]: LDA ($28),Y @ $17C6 = #$00 @ 送累加器
[00216BDB]7BDB [85 23 A2]: STA $23 = #$00 @ 存累加器
[00216BDD]7BDD [A2 00 86]: LDX #$00 @ 送变址寄存器X
[00216BDF]7BDF [86 24 A0]: STX $24 = #$00 @ 存变址寄存器X
[00216BE1]7BE1 [A0 00 B1]: LDY #$00 @ 送变址寄存器Y
[00216BE3]7BE3 [B1 28 85]: LDA ($28),Y @ $17C0 = #$00 @ 送累加器
[00216BE5]7BE5 [85 20 C8]: STA $20 = #$00 @ 存累加器
[00216BE7]7BE7 [C8 B1 28]: INY @ 变址寄存器Y加1
[00216BE8]7BE8 [B1 28 85]: LDA ($28),Y @ $17C1 = #$00 @ 送累加器
[00216BEA]7BEA [85 21 20]: STA $21 = #$00 @ 存累加器
[00216BEC]7BEC [20 F4 DC]: JSR $DCF4 @ 转子$00E88CF4
[00216BEF]7BEF [A0 00 A5]: LDY #$00 @ 送变址寄存器Y
[00216BF1]7BF1 [A5 20 91]: LDA $20 = #$00 @ 送累加器
[00216BF3]7BF3 [91 28 C8]: STA ($28),Y @ $17C0 = #$00 @ 存累加器
[00216BF5]7BF5 [C8 A5 21]: INY @ 变址寄存器Y加1
[00216BF6]7BF6 [A5 21 91]: LDA $21 = #$00 @ 送累加器
[00216BF8]7BF8 [91 28 A0]: STA ($28),Y @ $17C1 = #$00 @ 存累加器
[00216BFA]7BFA [A0 05 B1]: LDY #$05 @ 送变址寄存器Y
[00216BFC]7BFC [B1 28 A0]: LDA ($28),Y @ $17C5 = #$08 @ 送累加器
[00216BFE]7BFE [A0 03 18]: LDY #$03 @ 送变址寄存器Y
[00216C00]7C00 [18 71 28]: CLC @ 清进位标志
[00216C01]7C01 [71 28 A0]: ADC ($28),Y @ $17C3 = #$FF @ 带进位加
[00216C03]7C03 [A0 0B 91]: LDY #$0B @ 送变址寄存器Y
[00216C05]7C05 [91 28 A0]: STA ($28),Y @ $17CB = #$07 @ 存累加器
[00216C07]7C07 [A0 04 B1]: LDY #$04 @ 送变址寄存器Y
[00216C09]7C09 [B1 28 A0]: LDA ($28),Y @ $17C4 = #$00 @ 送累加器
[00216C0B]7C0B [A0 02 18]: LDY #$02 @ 送变址寄存器Y
[00216C0D]7C0D [18 71 28]: CLC @ 清进位标志
[00216C0E]7C0E [71 28 A0]: ADC ($28),Y @ $17C2 = #$FF @ 带进位加
[00216C10]7C10 [A0 0C 91]: LDY #$0C @ 送变址寄存器Y
[00216C12]7C12 [91 28 A0]: STA ($28),Y @ $17CC = #$FF @ 存累加器
[00216C14]7C14 [A0 00 B1]: LDY #$00 @ 送变址寄存器Y
[00216C16]7C16 [B1 28 85]: LDA ($28),Y @ $17C0 = #$00 @ 送累加器
[00216C18]7C18 [85 23 C8]: STA $23 = #$00 @ 存累加器
[00216C1A]7C1A [C8 B1 28]: INY @ 变址寄存器Y加1
[00216C1B]7C1B [B1 28 85]: LDA ($28),Y @ $17C1 = #$00 @ 送累加器
[00216C1D]7C1D [85 24 A0]: STA $24 = #$00 @ 存累加器
[00216C1F]7C1F [A0 09 B1]: LDY #$09 @ 送变址寄存器Y
[00216C21]7C21 [B1 28 85]: LDA ($28),Y @ $17C9 = #$06 @ 送累加器
[00216C23]7C23 [85 20 C8]: STA $20 = #$06 @ 存累加器
[00216C25]7C25 [C8 B1 28]: INY @ 变址寄存器Y加1
[00216C26]7C26 [B1 28 85]: LDA ($28),Y @ $17CA = #$90 @ 送累加器
[00216C28]7C28 [85 21 18]: STA $21 = #$90 @ 存累加器
[00216C2A]7C2A [18 A5 20]: CLC @ 清进位标志
[00216C2B]7C2B [A5 20 65]: LDA $20 = #$06 @ 送累加器
[00216C2D]7C2D [65 23 85]: ADC $23 = #$00 @ 带进位加
[00216C2F]7C2F [85 20 A5]: STA $20 = #$06 @ 存累加器
[00216C31]7C31 [A5 21 65]: LDA $21 = #$90 @ 送累加器
[00216C33]7C33 [65 24 85]: ADC $24 = #$00 @ 带进位加
[00216C35]7C35 [85 21 A0]: STA $21 = #$90 @ 存累加器
[00216C37]7C37 [A0 0D A5]: LDY #$0D @ 送变址寄存器Y
[00216C39]7C39 [A5 20 91]: LDA $20 = #$06 @ 送累加器
[00216C3B]7C3B [91 28 C8]: STA ($28),Y @ $17CD = #$06 @ 存累加器
[00216C3D]7C3D [C8 A5 21]: INY @ 变址寄存器Y加1
[00216C3E]7C3E [A5 21 91]: LDA $21 = #$90 @ 送累加器
[00216C40]7C40 [91 28 A9]: STA ($28),Y @ $17CE = #$90 @ 存累加器
[00216C42]7C42 [A9 00 20]: LDA #$00 @ 送累加器
[00216C44]7C44 [20 AA DA]: JSR $DAAA @ 转子$00E88AAA
[00216C47]7C47 [AD 36 19]: LDA $1936 = #$00 @ 送累加器
[00216C4A]7C4A [85 20 AD]: STA $20 = #$00 @ 存累加器
[00216C4C]7C4C [AD 37 19]: LDA $1937 = #$00 @ 送累加器
[00216C4F]7C4F [85 21 20]: STA $21 = #$00 @ 存累加器
[00216C51]7C51 [20 CA DA]: JSR $DACA @ 转子$00E88ACA
[00216C54]7C54 [A0 10 B1]: LDY #$10 @ 送变址寄存器Y
[00216C56]7C56 [B1 28 85]: LDA ($28),Y @ $17CD = #$06 @ 送累加器
[00216C58]7C58 [85 20 C8]: STA $20 = #$06 @ 存累加器
[00216C5A]7C5A [C8 B1 28]: INY @ 变址寄存器Y加1
[00216C5B]7C5B [B1 28 85]: LDA ($28),Y @ $17CE = #$90 @ 送累加器
[00216C5D]7C5D [85 21 20]: STA $21 = #$90 @ 存累加器
[00216C5F]7C5F [20 CA DA]: JSR $DACA @ 转子$00E88ACA
[00216C62]7C62 [A0 11 B1]: LDY #$11 @ 送变址寄存器Y
[00216C64]7C64 [B1 28 20]: LDA ($28),Y @ $17CC = #$FF @ 送累加器
[00216C66]7C66 [20 AA DA]: JSR $DAAA @ 转子$00E88AAA
[00216C69]7C69 [A0 11 B1]: LDY #$11 @ 送变址寄存器Y
[00216C6B]7C6B [B1 28 20]: LDA ($28),Y @ $17CB = #$07 @ 送累加器
[00216C6D]7C6D [20 AA DA]: JSR $DAAA @ 转子$00E88AAA
[00216C70]7C70 [A0 0B B1]: LDY #$0B @ 送变址寄存器Y
[00216C72]7C72 [B1 28 20]: LDA ($28),Y @ $17C4 = #$00 @ 送累加器
[00216C74]7C74 [20 AA DA]: JSR $DAAA @ 转子$00E88AAA
[00216C77]7C77 [A0 0D B1]: LDY #$0D @ 送变址寄存器Y
[00216C79]7C79 [B1 28 A2]: LDA ($28),Y @ $17C5 = #$08 @ 送累加器
[00216C7B]7C7B [A2 89 86]: LDX #$89 @ 送变址寄存器X
[00216C7D]7C7D [86 26 A2]: STX $26 = #$89 @ 存变址寄存器X
[00216C7F]7C7F [A2 E8 86]: LDX #$E8 @ 送变址寄存器X
[00216C81]7C81 [86 27 20]: STX $27 = #$E8 @ 存变址寄存器X
[00216C83]7C83 [20 F6 D2]: JSR $D2F6 @ 转子$00E882F6
[00216C86]7C86 [08 78 18]: PHP @ 处理器状态压入堆栈
[00216C87]7C87 [78 18 A5]: SEI @ 置中断禁止位
[00216C88]7C88 [18 A5 28]: CLC @ 清进位标志
[00216C89]7C89 [A5 28 69]: LDA $28 = #$B8 @ 送累加器
[00216C8B]7C8B [69 08 85]: ADC #$08 @ 带进位加
[00216C8D]7C8D [85 28 A5]: STA $28 = #$C0 @ 存累加器
[00216C8F]7C8F [A5 29 69]: LDA $29 = #$17 @ 送累加器
[00216C91]7C91 [69 00 85]: ADC #$00 @ 带进位加
[00216C93]7C93 [85 29 28]: STA $29 = #$17 @ 存累加器
[00216C95]7C95 [28 08 78]: PLP @ 堆栈弹回处理器状态
[00216C96]7C96 [08 78 18]: PHP @ 处理器状态压入堆栈
[00216C97]7C97 [78 18 A5]: SEI @ 置中断禁止位
[00216C98]7C98 [18 A5 28]: CLC @ 清进位标志
[00216C99]7C99 [A5 28 69]: LDA $28 = #$C0 @ 送累加器
[00216C9B]7C9B [69 10 85]: ADC #$10 @ 带进位加
[00216C9D]7C9D [85 28 A5]: STA $28 = #$D0 @ 存累加器
[00216C9F]7C9F [A5 29 69]: LDA $29 = #$17 @ 送累加器
[00216CA1]7CA1 [69 00 85]: ADC #$00 @ 带进位加
[00216CA3]7CA3 [85 29 28]: STA $29 = #$17 @ 存累加器
[00216CA5]7CA5 [28 60 08]: PLP @ 堆栈弹回处理器状态
[00216CA6]7CA6 [60 08 78]: RTS @ 子程序返回